# Code Challenge #1

Created by Cameron Omiccioli

1.  Make sure you have Node locally installed.
2.  Download Gulp Command Line Interface to be able to use gulp in your Terminal.

    ```
      npm install gulp-cli -g
    ```

3.  After installing Gulp, run npm install in the project folder to download all the project dependencies. You'll find them in the node_modules/ folder.

    ```
      npm install
    ```

4.  Run gulp in the project folder to serve the project files using BrowserSync. Running gulp will compile the theme and open /index.html in your main browser.

    ```
      gulp
    ```

While the gulp command is running, files in the src/scss/, and src/js/ folders will be monitored for changes. Files from the src/scss/ folder will generate injected CSS.

Hit `CTRL`+`C` to terminate the `gulp` command. This will stop the local server from running.


## Challenge

- Please create a webpage with a navigation that changes background color on scroll (you can do this with plugins or vanilla/jquery)

- With a full height slideshow (You may use plugins, we use [https://kenwheeler.github.io/slick/](https://kenwheeler.github.io/slick/))

- We use Bootstrap 3 framework, so that is already included

- Please choose your own fonts (Google Fonts is perfectly fine) and images (choose whichever placeholders you want)

- On callout link hover, have background label extend to top while text stays in place

- Make sure page is WCAG compliant (beyond color contrast, semantics as well)

- Feeling adventurous? Have the login button open up a modal with a tab trap.

- Nothing needs to be inside the modal, it does not need to interact with any external resources, just make it appear and disappear.
